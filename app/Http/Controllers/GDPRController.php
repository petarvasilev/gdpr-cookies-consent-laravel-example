<?php

namespace App\Http\Controllers;

use App\Http\Requests\CookiesConsentRequest;
use Illuminate\Http\Request;

class GDPRController extends Controller
{
    public function cookiesConsent(Request $request, CookiesConsentRequest $cookiesConsentRequest) {
        return response()->json(['status' => 'OK', 'message' => 'Cookies preferences successfully saved.']);
    }
}
