<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CookiesConsentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $types = config('cookies.types');

        $rules = [];

        foreach ($types as $id => $details) {
            if ($details['required']) {
                $rules[$id] = 'required|in:1';
            } else {
                $rules[$id] = 'required|in:1,0';
            }
        }

        return $rules;
    }
}
