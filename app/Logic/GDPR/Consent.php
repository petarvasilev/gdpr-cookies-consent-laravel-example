<?php

namespace App\Logic\GDPR;

use Exception;
use Illuminate\Support\Facades\Cookie;

class Consent {

    /**
     * Checks if consent is given for a specific cookie type
     * e.g functional cookies
     *
     * @param $cookieType
     * @return bool
     */
    static function isGiven($cookieType)
    {
        $settings = Cookie::get(config('cookies.settings-cookie-name'));

        try {
            $settings = json_decode($settings, true);
        } catch (Exception $e) {
            logger('Invalid JSON in the settings cookie.');
            exit(1);
        }

        if ($settings['allowedCookies'][$cookieType] === 1) {
            return true;
        }

        return false;
    }

    /**
     * Saves the cookie consent settings
     *
     * @param $cookies
     */
    static function set($cookies)
    {
        $cookieValue = json_encode(['allowedCookies' => $cookies]);

        Cookie::queue(config('cookies.settings-cookie-name'), $cookieValue, config('cookies.default-cookie-expiration'));
    }
}