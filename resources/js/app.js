/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.jQuery = require('jquery');
window.$ = window.jQuery;
require('./bootstrap');

$(function () {
    $('#cookies-settings-link').on('click', function(event) {
        event.preventDefault();
        $('#cookies-consent-popup').modal('show');
    });

    if ($('body').hasClass('functional-cookies-consent-is-not-given')) {
        $('#cookies-consent-popup').modal('show');
    }
});
