$(function() {
    $('button[type=submit]').on('click', function(event) {
        if ($('body').hasClass('functional-cookies-consent-is-not-given')) {
            event.preventDefault();
            $('#cookies-consent-popup').modal({show: true});
            $('#cookies-alert').show();
        }
    });
});